package com.bancadigital.movil.webservices;

import com.bancadigital.movil.Constantes;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;

/**
 * Created by Victor Garcia on 30/11/16.
 * Email: pfc.vik@gmail.com
 */
public class DaoRequest {

    public static synchronized ServicioWebBean requestRetrofit(Observable<?> observable, final ServicioWebBean servicioWebBean) {
        observable
                .timeout(Constantes.TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onCompleted() {
                        // do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        servicioWebBean.setError(e);
                    }

                    @Override
                    public void onNext(Object o) {
                        servicioWebBean.setResponse(o);
                    }
                });

        return servicioWebBean;
    }
}
