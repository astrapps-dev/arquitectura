package com.bancadigital.movil.webservices;

import android.content.Context;

import com.bancadigital.movil.base.BaseService;

import rx.Observable;
import rx.Observer;

/**
 * Created by Victor Garcia on 30/11/16.
 * Email: pfc.vik@gmail.com
 */
public class ServicioWebBean
{
    private Observable<?> endpoint;
    private Object response;
    private Throwable error;

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    public Observable<?> getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Observable<?> endpoint) {
        this.endpoint = endpoint;
    }
}
