package com.bancadigital.movil.webservices;

import com.bancadigital.movil.modulos.acceso.bean.SolicitudSmsBean;
import com.bancadigital.movil.modulos.acceso.bean.UsuarioBean;
import com.bancadigital.movil.modulos.acceso.bean.VerificarSmsBean;
import com.bancadigital.movil.modulos.acceso.beanresponse.ConsultaUsuarioBeanResponse;
import com.bancadigital.movil.modulos.acceso.beanresponse.SolicitudSmsBeanResponse;
import com.bancadigital.movil.modulos.acceso.beanresponse.VerificaUsuarioBeanResponse;
import com.bancadigital.movil.modulos.acceso.beanresponse.VerificarSmsBeanResponse;
import com.bancadigital.movil.modulos.token.bean.TokenBean;
import com.bancadigital.movil.modulos.token.beanresponse.TokenBeanResponse;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface BancaMovilApi {

    @POST("rest/v1/all") @Json
    Observable<TokenBeanResponse> solicitarTokenFreja(@Body TokenBean tokenBean);

    @POST("rest/v1/all") @Json
    Observable<SolicitudSmsBeanResponse> solicitarSMS(@Body SolicitudSmsBean solicitudSmsBean);

    @POST("rest/v1/all") @Json
    Observable<VerificarSmsBeanResponse> verificarSMS(@Body VerificarSmsBean verificarSmsBean);

    @POST("rest/v1/all") @Json
    Observable<VerificaUsuarioBeanResponse> verificarUsuario(@Body UsuarioBean usuarioBean);

    @POST("rest/v1/all") @Json
    Observable<ConsultaUsuarioBeanResponse> verificarCredencialesUsuario(@Body UsuarioBean usuarioBean);

}
