package com.bancadigital.movil.ejemplos.listaprueba.vista.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bancadigital.movil.R;
import com.bancadigital.movil.ejemplos.listaprueba.beanresponse.ListaPruebaBeanResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Victor Garcia on 21/12/16.
 *
 * @Author: Victor Garcia @Email:
 * @Descripcion:
 */
public class ListaPruebaAdapter extends RecyclerView.Adapter<ListaPruebaAdapter.ViewHolder> {

    private Context context;
    List<ListaPruebaBeanResponse> items;
    ItemClickListener listener;

    public interface ItemClickListener {
        void onClick(ViewHolder holder, List<ListaPruebaBeanResponse> items, ListaPruebaBeanResponse item, int position);
    }

    public ListaPruebaAdapter(Context context, List<ListaPruebaBeanResponse> items, ItemClickListener listener){
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_listademo_demo, viewGroup, false);
        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{

        @BindView(R.id.imagen) ImageView imageView;
        @BindView(R.id.nombre) TextView nombreView;
        @BindView(R.id.descripcion) TextView descView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(this, items, items.get(getAdapterPosition()), getAdapterPosition());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
       // Aqui se setea cada item en el view.
        viewHolder.nombreView.setText(items.get(i).getNombre());
        viewHolder.descView.setText(items.get(i).getDescripcion());

        Picasso.with(context)
                .load(items.get(i).getImagen())
                .into(viewHolder.imageView);

    }
}