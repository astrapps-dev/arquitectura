package com.bancadigital.movil.ejemplos.listaprueba.vista.fragment;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bancadigital.movil.R;
import com.bancadigital.movil.base.BaseActivity;
import com.bancadigital.movil.base.BaseBeanResponse;
import com.bancadigital.movil.base.BaseFragment;
import com.bancadigital.movil.ejemplos.listaprueba.beanresponse.ListaPruebaBeanResponse;
import com.bancadigital.movil.ejemplos.listaprueba.vista.activity.ListaPruebaActivity;
import com.bancadigital.movil.ejemplos.listaprueba.vista.adapters.ListaPruebaAdapter;
import com.bancadigital.movil.modulos.acceso.bean.SolicitudSmsBean;
import com.bancadigital.movil.modulos.acceso.vista.activity.VerificacionSmsActivity;
import com.bancadigital.movil.modulos.acceso.vista.fragment.SolicitudSmsFragment;
import com.bancadigital.movil.utilidades.ActivityHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class ListaPruebaFragment extends BaseFragment implements ListaPruebaAdapter.ItemClickListener {

    // Inyeccion de Views
    @BindView(R.id.recycler) RecyclerView recyclerView;
    ListaPruebaAdapter adapter;


    public static final String EXTRA_BEAN = "solicitudSmsBean";

    public static ListaPruebaFragment newInstance(SolicitudSmsBean solicitudSmsBean) {
        ListaPruebaFragment fragment = new ListaPruebaFragment();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_BEAN, solicitudSmsBean);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SolicitudSmsBean solicitudSmsBean = (SolicitudSmsBean) getArguments().getSerializable(EXTRA_BEAN);

        //
        ArrayList<ListaPruebaBeanResponse> items = new ArrayList<>();
        for (int f=0; f < 100 ; f++){
            ListaPruebaBeanResponse demoBean = new ListaPruebaBeanResponse();
            demoBean.setNombre("Elemento: " + f);
            demoBean.setDescripcion("descrion del item numero: " + f);
            demoBean.setImagen("https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150");
            items.add(demoBean);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new ListaPruebaAdapter(activity, items, this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_prueba_lista;
    }

    @Override
    protected String getFragmentTitle() {
        return null;
    }

    @Override
    protected void OnClickView(View view) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onError(BaseBeanResponse error) {

    }

    @Override
    public void onSuccess(Object data) {

    }

    @Override
    public void onUnauthorized() {

    }

    @Override
    public void isOffline() {

    }

    @Override
    public void onClick(ListaPruebaAdapter.ViewHolder holder, List<ListaPruebaBeanResponse> items, ListaPruebaBeanResponse item, int position) {
        Intent intent = new Intent(activity, ListaPruebaActivity.class);
        intent.putExtra(DialogPruebaFragment.EXTRA_BEAN, item);
        startActivity(intent);
    }
}
