package com.bancadigital.movil.ejemplos.listaprueba.vista.activity;

import android.os.Bundle;

import com.bancadigital.movil.R;
import com.bancadigital.movil.base.BaseActivity;
import com.bancadigital.movil.ejemplos.listaprueba.beanresponse.ListaPruebaBeanResponse;
import com.bancadigital.movil.ejemplos.listaprueba.vista.fragment.DialogPruebaFragment;
import com.bancadigital.movil.ejemplos.listaprueba.vista.fragment.ListaPruebaFragment;
import com.bancadigital.movil.modulos.acceso.vista.fragment.SolicitudSmsFragment;

/**
 * Created by Victor Garcia on 30/12/16.
 *
 * @Author: Victor Garcia @Email:
 * @Descripcion:
 */
public class ListaPruebaActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba_lista);

        ListaPruebaBeanResponse listaPruebaBeanResponse = null;
        if(getIntent().getExtras() != null)
            listaPruebaBeanResponse = (ListaPruebaBeanResponse) getIntent().getExtras().getSerializable(DialogPruebaFragment.EXTRA_BEAN);

        // Fragment principal de la actividad
        DialogPruebaFragment fragmentInicial =  DialogPruebaFragment.newInstance(listaPruebaBeanResponse);
        onChangeFragment(fragmentInicial, R.id.contenedor_principal, true);
    }
}