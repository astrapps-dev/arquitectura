package com.bancadigital.movil.ejemplos.listaprueba.vista;

import com.bancadigital.movil.base.BaseBeanResponse;

/**
 * Created by Victor Garcia on 30/12/16.
 *
 * @Author: Victor Garcia @Email:
 * @Descripcion:
 */
public class DemoListaBean extends BaseBeanResponse {

    private String imagen;
    private String nombre;
    private String descripcion;

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
