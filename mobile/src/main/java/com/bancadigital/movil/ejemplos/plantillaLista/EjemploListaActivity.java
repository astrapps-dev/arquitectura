package com.bancadigital.movil.ejemplos.plantillaLista;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.bancadigital.movil.R;
import com.bancadigital.movil.base.BaseActivity;

import java.util.List;

import butterknife.BindView;

public class EjemploListaActivity extends BaseActivity implements RecyclerViewAdapter.ItemClickListener {

    @BindView(R.id.recycler) RecyclerView recycler;
    RecyclerViewAdapter recyclerViewAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ejemplo_activity_lista);


        // Se configura el adapter
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearLayoutManager);
        recyclerViewAdapter = new RecyclerViewAdapter(EjemploListaActivity.this, null, this);
        recycler.setAdapter(recyclerViewAdapter);
    }

    @Override
    public void onClick(RecyclerViewAdapter.ViewHolder holder, List<Object> items, Object item, int position) {

    }
}
