package com.bancadigital.movil.ejemplos.listaprueba.vista.fragment;

import android.graphics.ImageFormat;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bancadigital.movil.R;
import com.bancadigital.movil.base.BaseActivity;
import com.bancadigital.movil.base.BaseBeanResponse;
import com.bancadigital.movil.base.BaseFragment;
import com.bancadigital.movil.ejemplos.listaprueba.beanresponse.ListaPruebaBeanResponse;
import com.bancadigital.movil.ejemplos.listaprueba.vista.adapters.ListaPruebaAdapter;
import com.bancadigital.movil.modulos.acceso.bean.SolicitudSmsBean;
import com.bancadigital.movil.modulos.acceso.vista.fragment.SolicitudSmsFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


public class DialogPruebaFragment extends BaseFragment {

    // Inyeccion de Views
    @BindView(R.id.imagen) ImageView imageView;
    @BindView(R.id.nombre) TextView nombreView;
    @BindView(R.id.descripcion) TextView descView;

    public static final String EXTRA_BEAN = "listaPruebaBeanResponse";

    public static DialogPruebaFragment newInstance(ListaPruebaBeanResponse listaPruebaBeanResponse) {
        DialogPruebaFragment fragment = new DialogPruebaFragment();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_BEAN, listaPruebaBeanResponse);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListaPruebaBeanResponse listaPruebaBeanResponse = (ListaPruebaBeanResponse) getArguments().getSerializable(EXTRA_BEAN);

        if (listaPruebaBeanResponse != null)
            setDataInViews(listaPruebaBeanResponse);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_prueba_dialog;
    }

    @Override
    protected String getFragmentTitle() {
        return null;
    }

    @Override
    @OnClick({R.id.buttonLista})
    protected void OnClickView(View view) {
        switch (view.getId()){
            case R.id.buttonLista:
                abrirPruebaDialog();
                break;
        }
    }

    private void abrirPruebaDialog(){
        ListaPruebaFragment fragment = ListaPruebaFragment.newInstance(new SolicitudSmsBean());

        fragmentListener.showDialogFragment(fragment, new BaseActivity.DialogFragmentInterface() {
            @Override
            public void onDialogFragmentResponse(Object object) {

            }
        });

    }

    private void setDataInViews(ListaPruebaBeanResponse listaPruebaBeanResponse){
        nombreView.setText(listaPruebaBeanResponse.getNombre());
        descView.setText(listaPruebaBeanResponse.getDescripcion());

        Picasso.with(activity)
                .load(listaPruebaBeanResponse.getImagen())
                .into(imageView);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onError(BaseBeanResponse error) {

    }

    @Override
    public void onSuccess(Object data) {

    }

    @Override
    public void onUnauthorized() {

    }

    @Override
    public void isOffline() {

    }
}
