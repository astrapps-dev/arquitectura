package com.bancadigital.movil.ejemplos.plantillaLista;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bancadigital.movil.R;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Victor Garcia on 21/12/16.
 *
 * @Author: Victor Garcia @Email:
 * @Descripcion:
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context context;
    List<Object> items;
    ItemClickListener listener;

    public interface ItemClickListener {
        void onClick(ViewHolder holder, List<Object> items, Object item, int position);
    }

    public RecyclerViewAdapter(Context context, List<Object> items, ItemClickListener listener){
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ejemplo_item_lista, viewGroup, false);
        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{



        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(this, items, items.get(getAdapterPosition()), getAdapterPosition());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }



    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
       // Aqui se setea cada item en el view.

    }
}