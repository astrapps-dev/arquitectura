package com.bancadigital.movil.ejemplos;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.bancadigital.movil.R;
import com.bancadigital.movil.base.BaseActivity;
import com.bancadigital.movil.ejemplos.listaprueba.beanresponse.ListaPruebaBeanResponse;

import java.util.ArrayList;

import butterknife.BindView;

public class EjemploViewsActivity extends BaseActivity {

    @BindView(R.id.spinner) Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejemplo);


        implementarSpinner();
    }

    private void implementarSpinner(){

        // Creamos la lista para el spinner.
        final ArrayList<String> items = new ArrayList<>();
        for (int f=0; f < 10 ; f++){
            items.add("Elemento: " + f);
        }


        // Creamos el array adapter para el spinner, pasando primero el contexto,
        // luego el layout de cada item y luego el arraylist de los elementos.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, items);
        // Especificamos el layout del spinner (dialogo o lista deslizable)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Aplicamos el adapter al spinner
        spinner.setAdapter(adapter);

        // Le aplicamos la interface para detectar cuando seleccionan un item.
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // An item was selected. You can retrieve the selected item using
                Toast.makeText(EjemploViewsActivity.this, items.get(i), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Another interface callback
            }
        });
    }
}
