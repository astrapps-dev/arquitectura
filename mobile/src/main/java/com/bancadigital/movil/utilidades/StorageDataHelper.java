package com.bancadigital.movil.utilidades;

import com.orhanobut.hawk.Hawk;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Victor Garcia on 17/11/16.
 * Email: pfc.vik@gmail.com
 */

// Conexion para la libreria de hawk en desarrollo.
public class StorageDataHelper {

    public static void put(String key, Object value){
        Hawk.put(key, value);
    }

    public static Object get(String key){
        return Hawk.get(key);
    }

    public static void getObservable(String key, Subscriber<? super Object> subscriber){
        Hawk.getObservable(key)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    public static void remove(String key){
        Hawk.remove(key);
    }

    public static boolean contains(String key){
        return Hawk.contains(key);
    }

}
