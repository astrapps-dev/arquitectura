package com.bancadigital.movil.utilidades;

import com.bancadigital.movil.Constantes;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Created by mareyes on 9/11/15.
 */
public class Fechas {




        public interface FORMATO {
            String DD_MM_AAAA_Diag = "dd/MM/yyyy";
            String DD_MMM_AAAA_Diag = "dd/MMM/yyyy";
            String DD_MM_AAAA_Guion = "dd-MM-yyyy";
            String DD_MMM_AAAA_Guion = "dd-MMM-yyyy";
            String DD_MMMM_AAAA      = "dd 'de' MMMMM 'del' yyyy";
            String DD_MMMM_AAAACorto = "dd 'de' MMMMM 'del' yyyy";
            String HHmm = "HH:mm";
            String HHmmss = "HH:mm:ss";
            String HHmmssSS = "HH:mm:ss:SS";
            String AAAA_MM_DD_Guion = "yyyy-MM-dd";
            String AAAA_MMM_DD_Guion = "yyyy-MMM-dd";
            String AAAA_MM_DD_Diag = "yyyy/MM/dd";
            String AAAA_MMM_DD_Diag = "yyyy/MMM/dd";
            String AAAA_MM_DD_SinGuion = "yyyyMMdd";
            String AAAA_MM_DD_HHmmssSSZ="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
            String AAAA_MM_DD_HHmmss_Guion ="yyyy-MM-dd HH:mm:ss";

        }

        public Fechas() {
            super();
        }


        public static String convierteStr(java.util.Date fecha, String formato)
        {

            SimpleDateFormat fechaFormato = new SimpleDateFormat(formato);
            String fechaStr = Constantes.CADENA_VACIA;
            /**Validamos si lo que entra es una fecha vacia*/

            if (fecha != null) {
                fechaStr = fechaFormato.format(fecha);
            }
            else {
                fechaStr = Constantes.FECHA_VACIA;
            }

            return fechaStr;
        }

        public static Date convierteDate(String fechaStr, String formato) {
            SimpleDateFormat fechaEntrada= new SimpleDateFormat(formato);

            Date fecha = null;

            fechaEntrada.setLenient(false);
            try {
                fecha = fechaEntrada.parse(fechaStr);
            } catch (Exception e) {


            }
            return fecha;
        }

        public static XMLGregorianCalendar convierteXMLGregorianCalendar(String fechaStr, String formato) {
            Date fecha =convierteDate(fechaStr,formato);

            GregorianCalendar calendario = new GregorianCalendar();
            XMLGregorianCalendar xmlCalendario = null;

            calendario.setTime(fecha);

            try {
                xmlCalendario = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendario);
                return xmlCalendario;
            } catch (DatatypeConfigurationException ex) {
                return null;
            }
        }


        public static XMLGregorianCalendar convierteXMLGregorianCalendar(Date fechaStr) {
            Date fecha =fechaStr;

            GregorianCalendar calendario = new GregorianCalendar();
            XMLGregorianCalendar xmlCalendario = null;

            calendario.setTime(fecha);

            try {
                xmlCalendario = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendario);
                return xmlCalendario;
            } catch (DatatypeConfigurationException ex) {
                return null;
            }
        }

        public static String getFechaActual(String formato) {
            Date ahora = new Date();
            SimpleDateFormat formateador = new SimpleDateFormat(formato);
            return formateador.format(ahora);
        }


    // Suma o resta las horas recibidos a la fecha
    public static Date sumarRestarHorasFecha(Date fecha, int horas){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.HOUR, horas);  // numero de horas a añadir, o restar en caso de horas<0
        return calendar.getTime(); // Devuelve el objeto Date con las nuevas horas añadidas

    }

    // Suma o resta los minutos recibidos a la fecha
    public static Date sumarRestarMinutosFecha(Date fecha, int minutos)throws Exception {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(fecha); // Configuramos la fecha que se recibe
            calendar.add(Calendar.MINUTE, minutos);  // numero de horas a añadir, o restar en caso de horas<0
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Fechas->sumarRestarMinutosFecha-->(Exception), error al sumar o restar fechas");
        }
        return calendar.getTime(); // Devuelve el objeto Date con las nuevas horas añadidas

    }


    }
