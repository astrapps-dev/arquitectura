package com.bancadigital.movil.utilidades;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by Victor Garcia on 17/11/16.
 * Email: pfc.vik@gmail.com
 */
public class ActivityHelper {

    public static void startActivity(Activity activity, Class<?> cls){
        Intent intent = new Intent(activity, cls);
        activity.startActivity(intent);
    }

    public static void startActivityFinish(Activity activity, Class<?> cls){
        Intent intent = new Intent(activity, cls);
        activity.startActivity(intent);
        activity.finish();
    }

    public static void startActivityClear(Activity activity, Class<?> cls){
        Intent intent = new Intent(activity, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(intent);
        activity.finish();
    }
}
