package com.bancadigital.movil.utilidades.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by Victor Garcia on 16/12/16.
 *
 * @Author: Victor Garcia @Email:
 * @Descripcion:
 */
public class SmsReceiver extends BroadcastReceiver {

    static SmsListener mlistener;

    public interface SmsListener {
        void messageReceived(String messageText);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle intentExtras = intent.getExtras();

        if (intentExtras != null) {
            /* Get Messages */
            Object[] sms = (Object[]) intentExtras.get("pdus");

            for (int i = 0; i < sms.length; ++i) {
                /* Parse Each Message */
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);

                String phone = smsMessage.getOriginatingAddress();
                String message = smsMessage.getMessageBody().toString();

                Toast.makeText(context, phone + ": " + message, Toast.LENGTH_SHORT).show();

                mlistener.messageReceived("codigo");
            }
        }
    }

    public static void bindListener(SmsListener listener) {
        mlistener = listener;
    }

}
