package com.bancadigital.movil.utilidades;

import android.support.design.widget.TextInputLayout;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bancadigital.movil.BancaMovilApplication;
import com.bancadigital.movil.R;

/**
 * Created by Victor Garcia on 28/11/16.
 * Email: pfc.vik@gmail.com
 */
public class ValidateViewsUtility {

    public static boolean isValid(CharSequence charSequence, boolean validFormat, View errorContainer, String errorMessage, String mEmptyMessage){
        return valid(validFormat, errorContainer, charSequence, errorMessage, mEmptyMessage);
    }

    public static boolean isValidPhoneNumber(CharSequence charSequence, View errorContainer){
        return valid(Patterns.PHONE.matcher(charSequence).matches() && charSequence.length() == 10
                , errorContainer, charSequence,
                BancaMovilApplication.getContext().getResources().getString(R.string.error_invalid_phone),
                BancaMovilApplication.getContext().getResources().getString(R.string.error_empty));
    }

    public static boolean isValidSmsCode(CharSequence charSequence, View errorContainer){
        return valid(charSequence.length() == 6
                , errorContainer, charSequence,
                BancaMovilApplication.getContext().getResources().getString(R.string.error_invalid_sms_code),
                BancaMovilApplication.getContext().getResources().getString(R.string.error_empty));
    }

    public static boolean isEmailValid(CharSequence charSequence, View errorContainer) {
        return valid(Patterns.EMAIL_ADDRESS.matcher(charSequence).matches()
                , errorContainer, charSequence,
                BancaMovilApplication.getContext().getResources().getString(R.string.error_invalid_email),
                BancaMovilApplication.getContext().getResources().getString(R.string.error_empty));
    }

    public static boolean isPasswordValid(CharSequence charSequence, View errorContainer) {
        return valid(charSequence.length() > 4
                , errorContainer, charSequence,
                BancaMovilApplication.getContext().getResources().getString(R.string.error_invalid_password),
                BancaMovilApplication.getContext().getResources().getString(R.string.error_empty));
    }

    private static boolean valid(boolean validFormat, View errorContainer, CharSequence charSequence, String errorMessage, String mEmptyMessage){
        TextView textView = null;
        TextInputLayout textInputLayout = null;
        String error;
        boolean errorResult;
        if (errorContainer instanceof EditText) {
            textView = (TextView) errorContainer;
        } else if (errorContainer instanceof TextInputLayout) {
            textInputLayout = (TextInputLayout) errorContainer;
        } else {
            throw new IllegalArgumentException("The View errorContainer must be a EditText or TextInputLayout");
        }

        if (mEmptyMessage != null && (charSequence == null || charSequence.length() == 0)) {
            error = mEmptyMessage;
            errorResult = false;
        } else if (validFormat) {
            error = "";
            errorResult = true;
        } else {
            error = errorMessage;
            errorResult = false;
        }

        if (textView != null){
            textView.setError(error);
        }

        if (textInputLayout != null){
            textInputLayout.setError(error);
        }

        return errorResult;
    }
}