package com.bancadigital.movil;

import android.app.Application;
import android.content.Context;

import com.bancadigital.movil.webservices.BancaMovilApi;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.HawkBuilder;
import com.orhanobut.hawk.LogLevel;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BancaMovilApplication extends Application {

    private static BancaMovilApplication sInstance;

    private static Retrofit retrofit;

    public static Context getContext()
    {
        return sInstance;
    }


    public BancaMovilApplication()
    {
        sInstance = this;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        initRetrofitRx();
        initHawkStorage();

    }

    private void initRetrofitRx(){
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();
        retrofit = new Retrofit.Builder()
                .baseUrl(Constantes.BASE_URL_API)
                .addConverterFactory( GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();
    }

    private void initHawkStorage(){
        Hawk.init(this)
                .setEncryptionMethod(HawkBuilder.EncryptionMethod.MEDIUM)
                .setStorage(HawkBuilder.newSqliteStorage(this))
                .setLogLevel(LogLevel.FULL)
                .build();
    }

    public static BancaMovilApi getApi(){
        return retrofit.create(BancaMovilApi.class);
    }

    public static Retrofit getRetrofit(){
        return retrofit;
    }

}
