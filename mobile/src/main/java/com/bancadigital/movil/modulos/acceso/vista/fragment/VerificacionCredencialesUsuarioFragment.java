package com.bancadigital.movil.modulos.acceso.vista.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bancadigital.movil.R;
import com.bancadigital.movil.modulos.acceso.bean.UsuarioBean;
import com.bancadigital.movil.modulos.acceso.beanresponse.VerificaUsuarioBeanResponse;
import com.bancadigital.movil.modulos.acceso.tareas.VerificarCredencialesUsuarioTarea;
import com.bancadigital.movil.modulos.inicio.vista.activity.InicioActivity;
import com.bancadigital.movil.base.BaseBeanResponse;
import com.bancadigital.movil.base.BaseFragment;
import com.bancadigital.movil.sessions.SessionDeviceBean;
import com.bancadigital.movil.sessions.SessionsManager;
import com.bancadigital.movil.utilidades.ActivityHelper;
import com.bancadigital.movil.utilidades.ValidateViewsUtility;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

public class VerificacionCredencialesUsuarioFragment extends BaseFragment {

    // Inyeccion de Views
    @BindView(R.id.frase_bienvenida) TextView fraseBienvenida;
    @BindView(R.id.imagen_antiphi) ImageView imagenAntiphi;
    @BindView(R.id.password_layout) TextInputLayout passwordLayout;
    @BindView(R.id.password) TextInputEditText passwordView;
    @BindView(R.id.progress) ProgressBar progress;
    @BindView(R.id.form) View form;

    // Extra Args
    private static final String EXTRA_USUARIO_BEAN = "extra_usuario_bean";
    private VerificaUsuarioBeanResponse verificaUsuarioBeanResponse;

    public static VerificacionCredencialesUsuarioFragment newInstance(VerificaUsuarioBeanResponse verificaUsuarioBeanResponse) {
        VerificacionCredencialesUsuarioFragment fragment = new VerificacionCredencialesUsuarioFragment();

        if (verificaUsuarioBeanResponse == null) {
            throw new IllegalArgumentException("Must pass EXTRA_USUARIO_BEAN");
        }

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_USUARIO_BEAN, verificaUsuarioBeanResponse);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        verificaUsuarioBeanResponse = (VerificaUsuarioBeanResponse) getArguments().getSerializable(EXTRA_USUARIO_BEAN);
        if (verificaUsuarioBeanResponse == null) {
            throw new IllegalArgumentException("Must pass EXTRA_USUARIO_BEAN");
        }

        setViewValues();
    }

    private void setViewValues(){
        // Frase bienvenida
        fraseBienvenida.setText(verificaUsuarioBeanResponse.getFraseBienvenida());
        // image
        Picasso.with(activity)
                .load(verificaUsuarioBeanResponse.getImgAntiphishing())
                .into(imagenAntiphi);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_acceso_verificacion_usuario_credenciales;
    }

    @Override
    protected String getFragmentTitle() {
        return getString(R.string.title_verificacion_credenciales_usuario);
    }

    @Override
    @OnClick({R.id.ingresar_button})
    public void OnClickView(View view) {
        switch (view.getId()){
            case R.id.ingresar_button:
                validarDatosViews();
                break;
        }
    }

    private void validarDatosViews(){

        // Guarda los valores en variables
        SessionDeviceBean sessionDeviceBean = SessionsManager.getCurrentDeviceSession();
        String telefono = sessionDeviceBean.getTelefono();
        String password = passwordView.getText().toString();

        // Valida los datos ingresados en formularios
        boolean passwordValid = ValidateViewsUtility.isPasswordValid(password, passwordLayout
        );

        if (passwordValid) {

            // Llama al servicio para verificar el usuario
            UsuarioBean usuarioBean = new UsuarioBean();
            usuarioBean.setTelefono(telefono);
            usuarioBean.setPassword(password);
            usuarioBean.setFolioSerial(verificaUsuarioBeanResponse.getFolioToken());

            VerificarCredencialesUsuarioTarea verificarCredencialesUsuarioTarea = new VerificarCredencialesUsuarioTarea(this, usuarioBean);
            verificarCredencialesUsuarioTarea.execute();
        }
    }

    @Override
    public void showProgress() {
        super.showProgressView(true, form, progress);
    }

    @Override
    public void hideProgress() {
        super.showProgressView(false, form, progress);
    }

    @Override
    public void onError(BaseBeanResponse error) {

    }

    @Override
    public void onSuccess(Object data) {

        // Pasar a la actividad principal
        ActivityHelper.startActivityClear(activity, InicioActivity.class);
    }

    @Override
    public void onUnauthorized() {

    }

    @Override
    public void isOffline() {

    }
}
