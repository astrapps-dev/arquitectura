package com.bancadigital.movil.modulos.acceso.beanresponse;

import com.bancadigital.movil.base.BaseBeanResponse;

/**
 * Created by Victor Garcia on 16/12/16.
 *
 * @Author: Victor Garcia
 * @Email: pfc.vik@gmail.com
 * @Descripcion:
 */
public class ConsultaUsuarioBeanResponse extends BaseBeanResponse {

    private String usuarioID;
    private String clienteID;
    private String nombreCompleto;
    private String fechaUltimoAcceso;
    private String perfilID;
    private String accesoConToken;
    private String transcToken;

    public ConsultaUsuarioBeanResponse() {
    }

    public String getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(String usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getClienteID() {
        return clienteID;
    }

    public void setClienteID(String clienteID) {
        this.clienteID = clienteID;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getFechaUltimoAcceso() {
        return fechaUltimoAcceso;
    }

    public void setFechaUltimoAcceso(String fechaUltimoAcceso) {
        this.fechaUltimoAcceso = fechaUltimoAcceso;
    }

    public String getPerfilID() {
        return perfilID;
    }

    public void setPerfilID(String perfilID) {
        this.perfilID = perfilID;
    }

    public String getAccesoConToken() {
        return accesoConToken;
    }

    public void setAccesoConToken(String accesoConToken) {
        this.accesoConToken = accesoConToken;
    }

    public String getTranscToken() {
        return transcToken;
    }

    public void setTranscToken(String transcToken) {
        this.transcToken = transcToken;
    }
}
