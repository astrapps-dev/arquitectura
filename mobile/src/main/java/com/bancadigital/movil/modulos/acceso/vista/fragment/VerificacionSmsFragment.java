package com.bancadigital.movil.modulos.acceso.vista.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bancadigital.movil.R;
import com.bancadigital.movil.modulos.acceso.bean.SolicitudSmsBean;
import com.bancadigital.movil.modulos.acceso.bean.VerificarSmsBean;
import com.bancadigital.movil.modulos.acceso.tareas.VerificarSMSTarea;
import com.bancadigital.movil.modulos.acceso.vista.activity.VerificacionUsuarioActivity;
import com.bancadigital.movil.base.BaseBeanResponse;
import com.bancadigital.movil.base.BaseFragment;
import com.bancadigital.movil.utilidades.ActivityHelper;
import com.bancadigital.movil.utilidades.ValidateViewsUtility;
import com.bancadigital.movil.utilidades.listener.SmsReceiver;

import butterknife.BindView;
import butterknife.OnClick;

public class VerificacionSmsFragment extends BaseFragment {

    // Inyeccion de Views
    @BindView(R.id.error) TextView errorView;
    @BindView(R.id.codigo_layout) TextInputLayout codigoLayout;
    @BindView(R.id.codigo) TextInputEditText codigoView;
    @BindView(R.id.verificar_sms_button) Button verificarSmsButton;
    @BindView(R.id.progress) ProgressBar progress;
    @BindView(R.id.form) View form;

    // Extra Args
    private static final String EXTRA_SMS_BEAN = "extra_sms_bean";
    private SolicitudSmsBean solicitudSmsBean;

    // Variables

    public static VerificacionSmsFragment newInstance(SolicitudSmsBean solicitudSmsBean) {
        VerificacionSmsFragment fragment = new VerificacionSmsFragment();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_SMS_BEAN, solicitudSmsBean);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Se obtienen los extras
        solicitudSmsBean = (SolicitudSmsBean) getArguments().getSerializable(EXTRA_SMS_BEAN);

        obtenerCodigoSMS();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_acceso_verificacion_sms;
    }

    @Override
    protected String getFragmentTitle() {
        return getString(R.string.title_verificacion_sms);
    }

    @Override
    @OnClick({R.id.verificar_sms_button})
    public void OnClickView(View view) {
        switch (view.getId()){
            case R.id.verificar_sms_button:
                validarDatosViews();
                break;
        }
    }

    private void obtenerCodigoSMS(){
        SmsReceiver.bindListener(new SmsReceiver.SmsListener() {
            @Override
            public void messageReceived(String messageText) {

                // Se valida si el usuario ha escrito el codigo
                if (codigoView.getText().toString().isEmpty()){
                    codigoView.setText(messageText);
                }

                validarDatosViews();
            }
        });
    }

    private void validarDatosViews(){

        // Guarda los valores en variables
        String codigo = codigoView.getText().toString();

        // Valida los datos ingresados en formularios
        boolean codeValid = ValidateViewsUtility.isValidSmsCode(codigo, codigoLayout
        );

        if (codeValid){

            // Se crean el bean
            VerificarSmsBean verificarSmsBean = new VerificarSmsBean();
            verificarSmsBean.setClienteID("");
            verificarSmsBean.setCodigoActivacion(codigo);
            verificarSmsBean.setNumTrasaccion("");
            verificarSmsBean.setTelefono(solicitudSmsBean.getTelefonoDestino());

            // Se llama a la tarea
            VerificarSMSTarea verificarSMSTarea = new VerificarSMSTarea(this, verificarSmsBean);
            verificarSMSTarea.execute();

        }
    }



    @Override
    public void showProgress() {
        super.showProgressView(true, form, progress);
    }

    @Override
    public void hideProgress() {
        super.showProgressView(false, form, progress);
    }

    @Override
    public void onError(BaseBeanResponse error) {
        errorView.setText(error.getMensajeRespuestaWS());
    }

    @Override
    public void onSuccess(Object data) {

        // Se pasa a la siguiente actividad "Verificacion de usuario"
        ActivityHelper.startActivityClear(activity, VerificacionUsuarioActivity.class);
    }

    @Override
    public void onUnauthorized() {

    }

    @Override
    public void isOffline() {
        errorView.setText(getString(R.string.error_offline));
    }
}
