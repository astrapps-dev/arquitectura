package com.bancadigital.movil.modulos.acceso.vista.activity;

import android.os.Bundle;
import android.support.annotation.IdRes;

import com.bancadigital.movil.R;
import com.bancadigital.movil.modulos.acceso.vista.fragment.SolicitudSmsFragment;
import com.bancadigital.movil.base.BaseActivity;
import com.bancadigital.movil.base.BaseFragment;
import com.bancadigital.movil.base.FragmentListener;

public class VerificacionSmsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba_lista);

        // Fragment principal de la actividad
        SolicitudSmsFragment fragmentInicial =  SolicitudSmsFragment.newInstance();
        onChangeFragment(fragmentInicial, R.id.contenedor_principal, true);
    }
}
