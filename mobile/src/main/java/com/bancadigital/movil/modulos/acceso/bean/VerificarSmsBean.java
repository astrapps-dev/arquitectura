package com.bancadigital.movil.modulos.acceso.bean;

import com.bancadigital.movil.base.BaseBean;

/**
 * Created by Victor Garcia on 16/12/16.
 *
 * @Author: Victor Garcia
 * @Email: pfc.vik@gmail.com
 * @Descripcion:
 */
public class VerificarSmsBean extends BaseBean{

    private String numTrasaccion;
    private String clienteID;
    private String codigoActivacion;
    private String telefono;

    public VerificarSmsBean() {
    }

    public String getNumTrasaccion() {
        return numTrasaccion;
    }

    public void setNumTrasaccion(String numTrasaccion) {
        this.numTrasaccion = numTrasaccion;
    }

    public String getClienteID() {
        return clienteID;
    }

    public void setClienteID(String clienteID) {
        this.clienteID = clienteID;
    }

    public String getCodigoActivacion() {
        return codigoActivacion;
    }

    public void setCodigoActivacion(String codigoActivacion) {
        this.codigoActivacion = codigoActivacion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
