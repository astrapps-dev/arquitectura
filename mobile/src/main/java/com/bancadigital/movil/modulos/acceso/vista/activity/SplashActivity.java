package com.bancadigital.movil.modulos.acceso.vista.activity;

import android.os.Bundle;

import com.bancadigital.movil.base.BaseActivity;
import com.bancadigital.movil.sessions.SessionDeviceBean;
import com.bancadigital.movil.sessions.SessionsManager;
import com.bancadigital.movil.utilidades.ActivityHelper;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends BaseActivity {

    private static final long SPLASH_SCREEN_DELAY = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SessionDeviceBean sessionDeviceBean = SessionsManager.getCurrentDeviceSession();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                if (sessionDeviceBean != null && sessionDeviceBean.getTelefono() != null)
                    ActivityHelper.startActivity(SplashActivity.this, VerificacionUsuarioActivity.class);
                else
                    ActivityHelper.startActivity(SplashActivity.this, VerificacionSmsActivity.class);

            }
        };
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }
}
