package com.bancadigital.movil.modulos.acceso.beanresponse;

import com.bancadigital.movil.base.BaseBeanResponse;

/**
 * Created by Victor Garcia on 16/12/16.
 *
 * @Author: Victor Garcia
 * @Email: pfc.vik@gmail.com
 * @Descripcion:
 */
public class VerificaUsuarioBeanResponse extends BaseBeanResponse{

    private String fraseBienvenida;
    private String imgAntiphishing;
    private String folioToken;

    public VerificaUsuarioBeanResponse() {
    }

    public String getFraseBienvenida() {
        return fraseBienvenida;
    }

    public void setFraseBienvenida(String fraseBienvenida) {
        this.fraseBienvenida = fraseBienvenida;
    }

    public String getImgAntiphishing() {
        return imgAntiphishing;
    }

    public void setImgAntiphishing(String imgAntiphishing) {
        this.imgAntiphishing = imgAntiphishing;
    }

    public String getFolioToken() {
        return folioToken;
    }

    public void setFolioToken(String folioToken) {
        this.folioToken = folioToken;
    }
}
