package com.bancadigital.movil.modulos.acceso.vista.activity;

import android.support.annotation.IdRes;
import android.os.Bundle;

import com.bancadigital.movil.R;
import com.bancadigital.movil.modulos.acceso.bean.SolicitudSmsBean;
import com.bancadigital.movil.modulos.acceso.vista.fragment.VerificacionUsuarioFragment;
import com.bancadigital.movil.base.BaseActivity;
import com.bancadigital.movil.base.BaseFragment;
import com.bancadigital.movil.base.FragmentListener;

public class VerificacionUsuarioActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceso_verificacion_usuario);


        // Fragment principal de la actividad
        VerificacionUsuarioFragment fragmentInicial =  new VerificacionUsuarioFragment();
        onChangeFragment(fragmentInicial, R.id.contenedor_principal, true);
    }
}
