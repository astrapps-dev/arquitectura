package com.bancadigital.movil.modulos.acceso.tareas;

import com.bancadigital.movil.modulos.acceso.bean.SolicitudSmsBean;
import com.bancadigital.movil.modulos.acceso.beanresponse.SolicitudSmsBeanResponse;
import com.bancadigital.movil.modulos.acceso.service.AccesoService;
import com.bancadigital.movil.base.BaseBeanResponse;
import com.bancadigital.movil.base.BaseFragment;
import com.bancadigital.movil.base.BaseTarea;
import com.bancadigital.movil.Constantes;

/**
 * Created by Victor Garcia on 5/12/16.
 * Email: pfc.vik@gmail.com
 */
public class SolicitarSMSTarea extends BaseTarea {

    // Variables standar de la tarea
    private AccesoService servicio = AccesoService.getInstance();
    private BaseFragment view;
    private Object bean;

    public SolicitarSMSTarea(BaseFragment view,
                             Object bean) {
        this.view = view;
        this.bean = bean;
    }

    @Override
    protected void onPreExecute() {
        // Se activa la barra de progreso
        view.showProgress();
    }

    @Override
    protected Object doInBackground(Object... params) {

        SolicitudSmsBeanResponse beanResponse;
        beanResponse = servicio.solicitarSMS((SolicitudSmsBean) bean);

        return beanResponse;
    }

    @Override
    protected void onPostExecute(Object result) {
        if (((BaseBeanResponse)result).getCodigoRespuestaWS().equals(Constantes.CODIGO_EXITO[0])){
            onSuccess(bean);
        } else {
            onError(result);
        }
    }

    @Override
    public void onSuccess(Object response) {
        view.onSuccess(response);
        view.hideProgress();
    }

    @Override
    public void onError(Object error) {
        BaseBeanResponse errorBean = (BaseBeanResponse) error;
        String e = errorBean.getCodigoRespuestaWS();
        if (e.equals(Constantes.CODIGO_ERROR_CONEXION[0])){
            view.isOffline();
        } else if (e.equals(Constantes.CODIGO_ERROR_AUTORIZACION[0])){
            view.onUnauthorized();
        } else if (e.equals(Constantes.CODIGO_ERROR_TIMEOUT[0])){
            view.isOffline();
        } else {
            view.onError(errorBean);
        }
        view.hideProgress();
    }
}
