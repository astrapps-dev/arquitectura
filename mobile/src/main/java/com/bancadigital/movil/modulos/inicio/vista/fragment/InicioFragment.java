package com.bancadigital.movil.modulos.inicio.vista.fragment;

import android.os.Bundle;
import android.view.View;

import com.bancadigital.movil.R;
import com.bancadigital.movil.base.BaseBeanResponse;
import com.bancadigital.movil.base.BaseFragment;

public class InicioFragment extends BaseFragment {

    public static InicioFragment newInstance() {
        InicioFragment fragment = new InicioFragment();

        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_inicio;
    }

    @Override
    protected String getFragmentTitle() {
        return null;
    }

    @Override
    protected void OnClickView(View view) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onError(BaseBeanResponse error) {

    }

    @Override
    public void onSuccess(Object data) {

    }

    @Override
    public void onUnauthorized() {

    }

    @Override
    public void isOffline() {

    }
}
