package com.bancadigital.movil.modulos.acceso.beanresponse;

import com.bancadigital.movil.base.BaseBeanResponse;

/**
 * Created by Victor Garcia on 16/12/16.
 *
 * @Author: Victor Garcia
 * @Email: pfc.vik@gmail.com
 * @Descripcion:
 */
public class SolicitudSmsBeanResponse extends BaseBeanResponse{

    private String numTrasaccion;

    public SolicitudSmsBeanResponse() {
    }

    public String getNumTrasaccion() {
        return numTrasaccion;
    }

    public void setNumTrasaccion(String numTrasaccion) {
        this.numTrasaccion = numTrasaccion;
    }
}
