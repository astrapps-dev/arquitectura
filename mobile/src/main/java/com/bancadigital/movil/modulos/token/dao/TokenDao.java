/*
 *
 *  * Copyright (C) 2014 Antonio Leiva Gordillo.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.bancadigital.movil.modulos.token.dao;

import com.bancadigital.movil.modulos.token.beanresponse.TokenBeanResponse;
import com.bancadigital.movil.base.BaseDao;
import com.bancadigital.movil.Constantes;
import com.bancadigital.movil.webservices.ServicioWebBean;

public class TokenDao extends BaseDao {

    private static TokenDao mInstance = null;

    public static TokenDao getInstance(){
        if(mInstance == null) {
            mInstance = new TokenDao();
        }
        return mInstance;
    }

    public TokenBeanResponse solicitarToken(ServicioWebBean servicioWebBean){

        TokenBeanResponse tokenBeanResponse = new TokenBeanResponse();
        servicioWebBean.setResponse(tokenBeanResponse);

        try {

            // TODO: Dejar solo esta linea cuando los WS tengan el formato correspondiente.
            tokenBeanResponse = (TokenBeanResponse) consultaServicio(servicioWebBean);

            // Datos de prueba
            tokenBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_EXITO[0]);
            tokenBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_EXITO[1]);

        } catch (Exception e){
            tokenBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_GENERAL[0]);
            tokenBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_GENERAL[1]);
        }

        return tokenBeanResponse;
    }
}
