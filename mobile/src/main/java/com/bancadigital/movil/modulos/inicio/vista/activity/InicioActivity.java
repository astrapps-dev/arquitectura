package com.bancadigital.movil.modulos.inicio.vista.activity;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.bancadigital.movil.R;
import com.bancadigital.movil.modulos.acceso.vista.fragment.SolicitudSmsFragment;
import com.bancadigital.movil.modulos.acceso.vista.fragment.VerificacionUsuarioFragment;
import com.bancadigital.movil.modulos.inicio.vista.fragment.InicioFragment;
import com.bancadigital.movil.base.BaseActivity;
import com.bancadigital.movil.base.BaseFragment;
import com.bancadigital.movil.base.FragmentListener;

import butterknife.BindView;

public class InicioActivity extends BaseActivity {

    @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        if (navigationView != null) {
            prepararDrawer(navigationView);
            seleccionarItem(navigationView.getMenu().getItem(0));
        }
    }

    //   Menu lateral
    private void prepararDrawer(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        seleccionarItem(menuItem);
                        drawerLayout.closeDrawer(GravityCompat.START);
                        return true;
                    }
                });

    }

    private void seleccionarItem(MenuItem itemDrawer) {
        BaseFragment fragmentoGenerico = null;

        switch (itemDrawer.getItemId()) {
            case R.id.nav_transferencias:
                fragmentoGenerico = new InicioFragment();
                break;
            case R.id.nav_pagos:
                fragmentoGenerico = new SolicitudSmsFragment();
                break;
            case R.id.nav_localizador:
                fragmentoGenerico = new SolicitudSmsFragment();
                break;
            case R.id.nav_sol_creditos:
                fragmentoGenerico = new SolicitudSmsFragment();
                break;
            case R.id.nav_bitacora:
                fragmentoGenerico = new SolicitudSmsFragment();
                break;
            case R.id.nav_config:
                fragmentoGenerico = new InicioFragment();
                break;
            case R.id.nav_ayuda:

                break;
            case R.id.nav_cerrar_sesion:

                break;
        }

        if (fragmentoGenerico != null) {
            onChangeFragment(fragmentoGenerico, R.id.contenedor_principal, true);
        }

        // Setear título actual
        setTitle(itemDrawer.getTitle());
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}
