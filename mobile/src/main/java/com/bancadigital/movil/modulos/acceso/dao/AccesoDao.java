/*
 *
 *  * Copyright (C) 2014 Antonio Leiva Gordillo.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.bancadigital.movil.modulos.acceso.dao;

import com.bancadigital.movil.modulos.acceso.beanresponse.ConsultaUsuarioBeanResponse;
import com.bancadigital.movil.modulos.acceso.beanresponse.SolicitudSmsBeanResponse;
import com.bancadigital.movil.modulos.acceso.beanresponse.VerificaUsuarioBeanResponse;
import com.bancadigital.movil.modulos.acceso.beanresponse.VerificarSmsBeanResponse;
import com.bancadigital.movil.base.BaseDao;
import com.bancadigital.movil.webservices.ServicioWebBean;
import com.bancadigital.movil.Constantes;

public class AccesoDao extends BaseDao {

    private static AccesoDao mInstance = null;

    public static AccesoDao getInstance(){
        if(mInstance == null) {
            mInstance = new AccesoDao();
        }
        return mInstance;
    }

    public SolicitudSmsBeanResponse solicitarSMS(ServicioWebBean servicioWebBean){

        SolicitudSmsBeanResponse solicitudSmsBeanResponse = new SolicitudSmsBeanResponse();
        servicioWebBean.setResponse(solicitudSmsBeanResponse);

        try {

            solicitudSmsBeanResponse = (SolicitudSmsBeanResponse) consultaServicio(servicioWebBean);

            // Datos de prueba
            solicitudSmsBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_EXITO[0]);
            solicitudSmsBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_EXITO[1]);
            solicitudSmsBeanResponse.setNumTrasaccion("001");

        } catch (Exception e){
            solicitudSmsBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_DAO[0]);
            solicitudSmsBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_DAO[1]);
        }

        return solicitudSmsBeanResponse;
    }

    public VerificarSmsBeanResponse verificarSMS(ServicioWebBean servicioWebBean){

        VerificarSmsBeanResponse verificarSmsBeanResponse = new VerificarSmsBeanResponse();
        servicioWebBean.setResponse(verificarSmsBeanResponse);

        try {

            verificarSmsBeanResponse = (VerificarSmsBeanResponse) consultaServicio(servicioWebBean);

            // Datos de prueba
            verificarSmsBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_EXITO[0]);
            verificarSmsBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_EXITO[1]);

        } catch (Exception e){
            verificarSmsBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_DAO[0]);
            verificarSmsBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_DAO[1]);
        }

        return verificarSmsBeanResponse;
    }

    public VerificaUsuarioBeanResponse verificarUsuario(ServicioWebBean servicioWebBean){

        VerificaUsuarioBeanResponse verificaUsuarioBeanResponse = new VerificaUsuarioBeanResponse();
        servicioWebBean.setResponse(verificaUsuarioBeanResponse);

        try {

            verificaUsuarioBeanResponse = (VerificaUsuarioBeanResponse) consultaServicio(servicioWebBean);

            // Datos de prueba
            verificaUsuarioBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_EXITO[0]);
            verificaUsuarioBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_EXITO[1]);
            verificaUsuarioBeanResponse.setFolioToken("001");
            verificaUsuarioBeanResponse.setFraseBienvenida("Frase Bienvenida");
            verificaUsuarioBeanResponse.setImgAntiphishing("https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150");

        } catch (Exception e){
            verificaUsuarioBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_DAO[0]);
            verificaUsuarioBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_DAO[1]);
        }

        return verificaUsuarioBeanResponse;
    }

    public ConsultaUsuarioBeanResponse verificarCredencialesUsuario(ServicioWebBean servicioWebBean){

        ConsultaUsuarioBeanResponse consultaUsuarioBeanResponse = new ConsultaUsuarioBeanResponse();
        servicioWebBean.setResponse(consultaUsuarioBeanResponse);

        try {

            consultaUsuarioBeanResponse = (ConsultaUsuarioBeanResponse) consultaServicio(servicioWebBean);

            // Datos de prueba
            consultaUsuarioBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_EXITO[0]);
            consultaUsuarioBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_EXITO[1]);
            consultaUsuarioBeanResponse.setAccesoConToken("001");
            consultaUsuarioBeanResponse.setClienteID("001");
            consultaUsuarioBeanResponse.setFechaUltimoAcceso("2016-12-21");
            consultaUsuarioBeanResponse.setNombreCompleto("Victor Garcia");
            consultaUsuarioBeanResponse.setPerfilID("001");
            consultaUsuarioBeanResponse.setUsuarioID("001");
            consultaUsuarioBeanResponse.setTranscToken("001");

        } catch (Exception e){
            consultaUsuarioBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_DAO[0]);
            consultaUsuarioBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_DAO[1]);
        }

        return consultaUsuarioBeanResponse;
    }
}
