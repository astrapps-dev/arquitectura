package com.bancadigital.movil.modulos.acceso.service;

import com.bancadigital.movil.modulos.acceso.bean.SolicitudSmsBean;
import com.bancadigital.movil.modulos.acceso.bean.UsuarioBean;
import com.bancadigital.movil.modulos.acceso.bean.VerificarSmsBean;
import com.bancadigital.movil.modulos.acceso.beanresponse.ConsultaUsuarioBeanResponse;
import com.bancadigital.movil.modulos.acceso.beanresponse.SolicitudSmsBeanResponse;
import com.bancadigital.movil.modulos.acceso.beanresponse.VerificaUsuarioBeanResponse;
import com.bancadigital.movil.modulos.acceso.beanresponse.VerificarSmsBeanResponse;
import com.bancadigital.movil.modulos.acceso.dao.AccesoDao;
import com.bancadigital.movil.base.BaseService;
import com.bancadigital.movil.sessions.SessionBean;
import com.bancadigital.movil.sessions.SessionDeviceBean;
import com.bancadigital.movil.sessions.SessionsManager;
import com.bancadigital.movil.webservices.ServicioWebBean;
import com.bancadigital.movil.Constantes;

public class AccesoService extends BaseService {

    private AccesoDao countriesDao = AccesoDao.getInstance();

    private static AccesoService mInstance = null;

    public static AccesoService getInstance(){
        if(mInstance == null) {
            mInstance = new AccesoService();
        }
        return mInstance;
    }

    public SolicitudSmsBeanResponse solicitarSMS(SolicitudSmsBean solicitudSmsBean){
        super.onStartService();

        SolicitudSmsBeanResponse solicitudSmsBeanResponse = new SolicitudSmsBeanResponse();

        ServicioWebBean servicioWebBean = new ServicioWebBean();
        servicioWebBean.setEndpoint(api.solicitarSMS(solicitudSmsBean));

        try {
            // Se valida el internet y retorna un booleano
            if (!isOnline) {
                solicitudSmsBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[0]);
                solicitudSmsBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[1]);
                return solicitudSmsBeanResponse;
            }

            // se llama al dao correspondiente del servicio
            solicitudSmsBeanResponse = countriesDao.solicitarSMS(servicioWebBean);


        } catch (Exception e){
            solicitudSmsBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_SERVICIO[0]);
            solicitudSmsBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_SERVICIO[1]);
        }

        return solicitudSmsBeanResponse;
    }

    public VerificarSmsBeanResponse verificarSMS(VerificarSmsBean verificarSmsBean){
        super.onStartService();

        VerificarSmsBeanResponse verificarSmsBeanResponse = new VerificarSmsBeanResponse();

        ServicioWebBean servicioWebBean = new ServicioWebBean();
        servicioWebBean.setEndpoint(api.verificarSMS(verificarSmsBean));

        try {
            // Se valida el internet y retorna un booleano
            if (!isOnline) {
                verificarSmsBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[0]);
                verificarSmsBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[1]);
                return verificarSmsBeanResponse;
            }

            // se llama al dao correspondiente del servicio
            verificarSmsBeanResponse = countriesDao.verificarSMS(servicioWebBean);

            if (verificarSmsBeanResponse.getCodigoRespuestaWS().equals(Constantes.CODIGO_EXITO[0])){

                // Se almacena el bean verificacion de sms en el telefono
                SessionDeviceBean sessionDeviceBean = new SessionDeviceBean();
                sessionDeviceBean.setTelefono(verificarSmsBean.getTelefono());

                SessionsManager.setDeviceSession(sessionDeviceBean);
            }


        } catch (Exception e){
            verificarSmsBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_SERVICIO[0]);
            verificarSmsBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_SERVICIO[1]);
        }

        return verificarSmsBeanResponse;
    }

    public VerificaUsuarioBeanResponse verificarUsuario(UsuarioBean usuarioBean){
        super.onStartService();

        VerificaUsuarioBeanResponse verificaUsuarioBeanResponse = new VerificaUsuarioBeanResponse();

        ServicioWebBean servicioWebBean = new ServicioWebBean();
        servicioWebBean.setEndpoint(api.verificarUsuario(usuarioBean));

        try {
            // Se valida el internet y retorna un booleano
            if (!isOnline) {
                verificaUsuarioBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[0]);
                verificaUsuarioBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[1]);
                return verificaUsuarioBeanResponse;
            }

            // se llama al dao correspondiente del servicio
            verificaUsuarioBeanResponse = countriesDao.verificarUsuario(servicioWebBean);


        } catch (Exception e){
            verificaUsuarioBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_SERVICIO[0]);
            verificaUsuarioBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_SERVICIO[1]);
        }

        return verificaUsuarioBeanResponse;
    }

    public ConsultaUsuarioBeanResponse verificarCredencialesUsuario(UsuarioBean usuarioBean){
        super.onStartService();

        ConsultaUsuarioBeanResponse consultaUsuarioBeanResponse = new ConsultaUsuarioBeanResponse();

        ServicioWebBean servicioWebBean = new ServicioWebBean();
        servicioWebBean.setEndpoint(api.verificarCredencialesUsuario(usuarioBean));

        try {
            // Se valida el internet y retorna un booleano
            if (!isOnline) {
                consultaUsuarioBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[0]);
                consultaUsuarioBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[1]);
                return consultaUsuarioBeanResponse;
            }

            // Autorizar Transaccion con Freja
            boolean transaccionFrejaAutorizada = autorizarTransaccionFreja(usuarioBean.getFolioSerial());

            // Se verifica si Freja autorizo la transaccion
            if (!transaccionFrejaAutorizada){
                consultaUsuarioBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_AUTORIZACION[0]);
                consultaUsuarioBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_AUTORIZACION[1]);
                return consultaUsuarioBeanResponse;
            }

            // se llama al dao correspondiente del servicio
            consultaUsuarioBeanResponse = countriesDao.verificarCredencialesUsuario(servicioWebBean);

            if (consultaUsuarioBeanResponse.getCodigoRespuestaWS().equals(Constantes.CODIGO_EXITO[0])){
                SessionBean sessionBean = new SessionBean();
                sessionBean.setUsuarioID(sessionBean.getUsuarioID());
                sessionBean.setClienteID(sessionBean.getClienteID());
                sessionBean.setNombreCompleto(sessionBean.getNombreCompleto());
                sessionBean.setFechaUltimoAcceso(sessionBean.getFechaUltimoAcceso());
                sessionBean.setPerfilID(sessionBean.getPerfilID());
                sessionBean.setAccesoToken(sessionBean.getAccesoToken());
                sessionBean.setTranscToken(sessionBean.getTranscToken());

                SessionsManager.createSession(sessionBean);
            }



        } catch (Exception e){
            consultaUsuarioBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_SERVICIO[0]);
            consultaUsuarioBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_SERVICIO[1]);
        }

        return consultaUsuarioBeanResponse;
    }
}
