package com.bancadigital.movil.modulos.acceso.vista.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bancadigital.movil.R;
import com.bancadigital.movil.modulos.acceso.bean.SolicitudSmsBean;
import com.bancadigital.movil.modulos.acceso.tareas.SolicitarSMSTarea;
import com.bancadigital.movil.base.BaseBeanResponse;
import com.bancadigital.movil.base.BaseFragment;
import com.bancadigital.movil.utilidades.ValidateViewsUtility;

import butterknife.BindView;
import butterknife.OnClick;

public class SolicitudSmsFragment extends BaseFragment {

    // Inyeccion de Views
    @BindView(R.id.error) TextView errorView;
    @BindView(R.id.telefono_layout) TextInputLayout telefonoLayout;
    @BindView(R.id.telefono) TextInputEditText telefonoView;
    @BindView(R.id.progress) ProgressBar progress;
    @BindView(R.id.form) View form;

    public static SolicitudSmsFragment newInstance() {
        SolicitudSmsFragment fragment = new SolicitudSmsFragment();

        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_acceso_solicitud_sms;
    }

    @Override
    protected String getFragmentTitle() {
        return getString(R.string.title_solicitud_sms);
    }

    @Override
    @OnClick({R.id.solicitar_sms_button})
    public void OnClickView(View view) {
        switch (view.getId()){
            case R.id.solicitar_sms_button:
                validarDatosViews();
                break;
        }
    }

    private void validarDatosViews(){

        // Guarda los valores en variables
        String telefono = telefonoView.getText().toString();

        // Valida los datos ingresados en formularios
        boolean phoneValid = ValidateViewsUtility.isValidPhoneNumber(telefono, telefonoLayout);

        if (phoneValid){

            // Llamar al servicio correspondiente
            SolicitudSmsBean verificacionUsuarioBean = new SolicitudSmsBean();

            verificacionUsuarioBean.setTelefonoDestino(telefono);
            verificacionUsuarioBean.setCampaniaID("TELCEL");

            SolicitarSMSTarea verificacionUsuarioTarea = new SolicitarSMSTarea(this, verificacionUsuarioBean);

            verificacionUsuarioTarea.execute();

        }
    }

    @Override
    public void showProgress() {
        super.showProgressView(true, form, progress);
    }

    @Override
    public void hideProgress() {
        super.showProgressView(false, form, progress);
    }

    @Override
    public void onError(BaseBeanResponse error) {
        errorView.setText(error.getMensajeRespuestaWS());
    }

    @Override
    public void onSuccess(Object data) {
        VerificacionSmsFragment verificacionSmsFragment = VerificacionSmsFragment.newInstance((SolicitudSmsBean) data);
        fragmentListener.onChangeFragment(verificacionSmsFragment, R.id.contenedor_principal, true);
    }

    @Override
    public void onUnauthorized() {

    }

    @Override
    public void isOffline() {
        errorView.setText(getString(R.string.error_offline));
    }
}
