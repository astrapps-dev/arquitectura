package com.bancadigital.movil.modulos.acceso.bean;

import com.bancadigital.movil.base.BaseBean;

/**
 * Created by Victor Garcia on 16/12/16.
 *
 * @Author: Victor Garcia
 * @email: pfc.vik@gmail.com
 * @Descripcion:
 */
public class SolicitudSmsBean extends BaseBean{

    private String telefonoDestino;
    private String campaniaID;

    public SolicitudSmsBean() {
    }

    public String getTelefonoDestino() {
        return telefonoDestino;
    }

    public void setTelefonoDestino(String telefonoDestino) {
        this.telefonoDestino = telefonoDestino;
    }

    public String getCampaniaID() {
        return campaniaID;
    }

    public void setCampaniaID(String campaniaID) {
        this.campaniaID = campaniaID;
    }
}
