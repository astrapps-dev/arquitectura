package com.bancadigital.movil.modulos.token.service;

import com.bancadigital.movil.modulos.token.bean.TokenBean;
import com.bancadigital.movil.modulos.token.beanresponse.TokenBeanResponse;
import com.bancadigital.movil.modulos.token.dao.TokenDao;
import com.bancadigital.movil.base.BaseService;
import com.bancadigital.movil.Constantes;
import com.bancadigital.movil.webservices.ServicioWebBean;

public class TokenService extends BaseService {

    private TokenDao tokenDao = TokenDao.getInstance();

    private static TokenService mInstance = null;

    public static TokenService getInstance(){
        if(mInstance == null) {
            mInstance = new TokenService();
        }
        return mInstance;
    }

    public TokenBeanResponse solicitarToken(TokenBean tokenBean){
        super.onStartService();

        TokenBeanResponse tokenBeanResponse = new TokenBeanResponse();

        ServicioWebBean servicioWebBean = new ServicioWebBean();
        servicioWebBean.setEndpoint(api.solicitarTokenFreja(tokenBean));

        try {
            // Se valida el internet y retorna un booleano
            if (!isOnline) {
                tokenBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[0]);
                tokenBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[1]);
                return tokenBeanResponse;
            }

            if (!hasAuthorization) {
                tokenBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_AUTORIZACION[0]);
                tokenBeanResponse.setMensajeRespuestaWS(Constantes.CODIGO_ERROR_AUTORIZACION[1]);
                return tokenBeanResponse;
            }

            // se llama al dao correspondiente del servicio
            tokenBeanResponse = tokenDao.solicitarToken(servicioWebBean);


        } catch (Exception e){
            tokenBeanResponse.setCodigoRespuestaWS(Constantes.CODIGO_ERROR_GENERAL[0]);
        }

        return tokenBeanResponse;
    }
}
