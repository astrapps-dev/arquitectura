package com.bancadigital.movil.modulos.acceso.bean;

import com.bancadigital.movil.base.BaseBean;

/**
 * Created by Victor Garcia on 16/12/16.
 *
 * @Author: Victor Garcia @Email:
 * @Descripcion:
 */
public class UsuarioBean extends BaseBean {

    private String telefono;
    private String password;

    public UsuarioBean() {
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
