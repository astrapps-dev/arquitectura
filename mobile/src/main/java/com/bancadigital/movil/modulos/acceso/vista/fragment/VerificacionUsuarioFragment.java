package com.bancadigital.movil.modulos.acceso.vista.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.bancadigital.movil.R;
import com.bancadigital.movil.modulos.acceso.bean.UsuarioBean;
import com.bancadigital.movil.modulos.acceso.beanresponse.VerificaUsuarioBeanResponse;
import com.bancadigital.movil.modulos.acceso.tareas.VerificarUsuarioTarea;
import com.bancadigital.movil.base.BaseBeanResponse;
import com.bancadigital.movil.base.BaseFragment;
import com.bancadigital.movil.sessions.SessionDeviceBean;
import com.bancadigital.movil.sessions.SessionsManager;
import com.bancadigital.movil.views.MaskedEditText;

import butterknife.BindView;
import butterknife.OnClick;

public class VerificacionUsuarioFragment extends BaseFragment {

    // Inyeccion de Views
    @BindView(R.id.telefono) MaskedEditText telefonoView;
    @BindView(R.id.progress) ProgressBar progress;
    @BindView(R.id.form) View form;

    // Variables de clase
    SessionDeviceBean sessionDeviceBean;

    public static VerificacionUsuarioFragment newInstance() {
        VerificacionUsuarioFragment fragment = new VerificacionUsuarioFragment();

        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sessionDeviceBean = SessionsManager.getCurrentDeviceSession();

        String telefono = sessionDeviceBean.getTelefono();
        telefonoView.setMaskedText(telefono.substring(6, 10));

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_acceso_verificacion_usuario;
    }

    @Override
    protected String getFragmentTitle() {
        return getString(R.string.title_verificacion_usuario);
    }

    @Override
    @OnClick({R.id.ingresar_button})
    public void OnClickView(View view) {
        switch (view.getId()){
            case R.id.ingresar_button:
                validarDatosViews();
                break;
        }
    }

    private void validarDatosViews(){

        // Guarda los valores en variables
        String telefono = sessionDeviceBean.getTelefono();

        // Llama al servicio para verificar el usuario
        UsuarioBean usuarioBean = new UsuarioBean();
        usuarioBean.setTelefono(telefono);

        VerificarUsuarioTarea verificarUsuarioTarea = new VerificarUsuarioTarea(this, usuarioBean);
        verificarUsuarioTarea.execute();
    }

    @Override
    public void showProgress() {
        super.showProgressView(true, form, progress);
    }

    @Override
    public void hideProgress() {
        super.showProgressView(false, form, progress);
    }

    @Override
    public void onError(BaseBeanResponse error) {

    }

    @Override
    public void onSuccess(Object data) {
        VerificacionCredencialesUsuarioFragment verificacionCredencialesUsuarioFragment =
                VerificacionCredencialesUsuarioFragment
                        .newInstance((VerificaUsuarioBeanResponse) data);
        fragmentListener.onChangeFragment(verificacionCredencialesUsuarioFragment, R.id.contenedor_principal, true);
    }

    @Override
    public void onUnauthorized() {

    }

    @Override
    public void isOffline() {

    }
}
