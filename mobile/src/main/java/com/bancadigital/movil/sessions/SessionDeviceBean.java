package com.bancadigital.movil.sessions;

/**
 * Objeto bean para el almacenamiento de la verificación sms de un usuario en el dispositivo.
 *
 * @author Victor Garcia
 * @email pfc.vik@gmail.com
 */
public class SessionDeviceBean {

    private String telefono;

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
