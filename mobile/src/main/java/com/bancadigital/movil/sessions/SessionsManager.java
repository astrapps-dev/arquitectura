package com.bancadigital.movil.sessions;

import com.bancadigital.movil.utilidades.StorageDataHelper;

/**
 * Clase creada para la administracion de sesiones de usuario en la aplicación.
 * Contiene los metodos para la creacion, actualizacion y elimacion de sesiones en el dispositivo.
 *
 * @author Victor Garcia
 * @email pfc.vik@gmail.com
 */
public class SessionsManager {

    /**
     *  Key para el almacenamiento de la sesion del usuario
     */
    private static final String SESSION_KEY = "sessionKeyHawk";

    /**
     *  Key para el almacenamiento de la verificacion sms del usuario
     */
    private static final String SESSION_PHONE_KEY = "sessionPhoneKeyHawk";

    /**
     * Metodo para almacenar cuando un usuario ya registro y verifico su cuenta con la solicitud
     * de sms, se almacena usando la key {@link #SESSION_PHONE_KEY}
     *
     * @param sessionDeviceBean Bean para almacenar la sesion de verificacion de sms
     */
    public static void setDeviceSession(SessionDeviceBean sessionDeviceBean){
        if (sessionDeviceBean == null)
            throw new NullPointerException("The SessionDeviceBean param should not be null");

        StorageDataHelper.put(SESSION_PHONE_KEY, sessionDeviceBean);
    }

    /**
     * Obtiene la sesion de verificacion de sms que se ha realizado en el telefono.
     *
     * @return se retorna un bean con la sesion.
     */
    public static SessionDeviceBean getCurrentDeviceSession(){

        SessionDeviceBean sessionDeviceBean = (SessionDeviceBean) StorageDataHelper.get(SESSION_PHONE_KEY);

        return sessionDeviceBean;
    }

    /**
     * Elimina la sesion de verificacion de sms.
     * De esta manera el usuario tendra que volver a verificar su telefono con sms.
     */
    public static void deleteDeviceSession(){
        StorageDataHelper.remove(SESSION_PHONE_KEY);
    }

    /**
     * Metodo para almacenar la sesion de un usuario, se almacena usando la key {@link #SESSION_KEY}
     *
     * @param sessionBean Bean para almacenar la sesion del usuario
     */
    public static void createSession(SessionBean sessionBean){
        if (sessionBean == null)
            throw new NullPointerException("The SessionBean param should not be null");

        StorageDataHelper.put(SESSION_KEY, sessionBean);
    }

    /**
     * Actualiza la informacion de la sesion de un usuario, utilizado cuando modifico alguna informacion de su cuenta.
     *
     * @param sessionBean se obiene el bean a actualizar.
     */
    public static void updateSession(SessionBean sessionBean){
        if (sessionBean == null)
            throw new NullPointerException("The SessionBean param should not be null");

        StorageDataHelper.put(SESSION_KEY, sessionBean);
    }

    /**
     * Elimina la sesion del usuario en el telefono.
     */
    public static void deleteSession(){
        StorageDataHelper.remove(SESSION_KEY);
    }

    /**
     * Obtiene la sesion del usuario. El bean contiene informacion basica de la cuenta.
     *
     * @return se retorna un bean con la sesion.
     */
    public static SessionBean getCurrentSession(){

        SessionBean sessionBean = (SessionBean) StorageDataHelper.get(SESSION_KEY);
        return sessionBean;
    }
}
