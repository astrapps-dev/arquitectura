package com.bancadigital.movil.sessions;

/**
 * Objeto bean para el almacenamiento de la sesion de un usuario en el dispositivo.
 *
 * @author Victor Garcia
 * @email pfc.vik@gmail.com
 */
public class SessionBean {

    private String usuarioID;
    private String clienteID;
    private String nombreCompleto;
    private String fechaUltimoAcceso;
    private String perfilID;
    private String accesoToken;
    private String transcToken;

    public String getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(String usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getClienteID() {
        return clienteID;
    }

    public void setClienteID(String clienteID) {
        this.clienteID = clienteID;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getFechaUltimoAcceso() {
        return fechaUltimoAcceso;
    }

    public void setFechaUltimoAcceso(String fechaUltimoAcceso) {
        this.fechaUltimoAcceso = fechaUltimoAcceso;
    }

    public String getPerfilID() {
        return perfilID;
    }

    public void setPerfilID(String perfilID) {
        this.perfilID = perfilID;
    }

    public String getAccesoToken() {
        return accesoToken;
    }

    public void setAccesoToken(String accesoToken) {
        this.accesoToken = accesoToken;
    }

    public String getTranscToken() {
        return transcToken;
    }

    public void setTranscToken(String transcToken) {
        this.transcToken = transcToken;
    }
}
