package com.bancadigital.movil;

/**
 * Constantes generales para el proyecto
 *
 * @author Victor Garcia
 * @email pfc.vik@gmail.com
 */
public class Constantes {

    // Constantes utilizadas para el proyecto en general
    public final  static String CADENA_VACIA    =   "";
    public final  static Integer ENTERO_VACIO   =   0;
    public final  static Double DOUBLE_CERO    =   0.00;
    public final  static String FECHA_VACIA   =   "1900-01-01";

    // Constantes de mensajes generales
    public static final String[] CODIGO_EXITO               = new String[]{"000","PETICION EXITOSA"};

    public static final String[] CODIGO_ERROR_GENERAL       = {"400","ERROR: SERVICIO WEB NO DISPONIBLE"};
    public static final String[] CODIGO_ERROR_CONEXION      = {"404","ERROR: ERROR DE CONEXION"};
    public static final String[] CODIGO_ERROR_AUTORIZACION  = {"407","ERROR: ERROR DE AUTORIZACION"};
    public static final String[] CODIGO_ERROR_TIMEOUT       = {"405","ERROR: ERROR POR TIMEOUT"};

    // Errores generales en las capas
    public static final String[] CODIGO_ERROR_SERVICIO       = {"002","ERROR: EN LA CAPA SERVICIO"};
    public static final String[] CODIGO_ERROR_DAO       = {"003","ERROR: EN LA CAPA DAO"};

    // Constantes de la API o WebService
    public static final String BASE_URL_API = "https://restcountries.eu/";
    public static final int TIMEOUT_SECONDS = 5;
}
