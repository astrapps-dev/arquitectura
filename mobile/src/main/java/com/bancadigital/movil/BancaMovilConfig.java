package com.bancadigital.movil;


import com.bancadigital.movil.BuildConfig;

public class BancaMovilConfig
{

	// debug logs, value is set via build config in build.gradle
	public static final boolean LOGS = BuildConfig.LOGS;
}
