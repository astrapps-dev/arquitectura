package com.bancadigital.movil.base;

import android.os.AsyncTask;

/**
 * Base Tarea creado para ser extendido por cada tarea en esta aplicación.
 * Necesario para la ejecucion de servicios syncronos en segundo plano.
 *
 * @author Victor Garcia
 * @email pfc.vik@gmail.com
 */
public abstract class BaseTarea extends AsyncTask<Object, Integer, Object> {

    @Override
    protected abstract void onPreExecute();

    @Override
    protected abstract Object doInBackground(Object... params);

    @Override
    protected abstract void onPostExecute(Object result);

    public abstract void onSuccess(Object response);

    public abstract void onError(Object error);

}
