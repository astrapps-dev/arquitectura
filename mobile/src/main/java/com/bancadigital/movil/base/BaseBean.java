package com.bancadigital.movil.base;

import java.io.Serializable;

/**
 * Base Bean creado para ser implementado por todos los Bean en la aplicacion.
 * Contiene variables comunes como regla de negocio.
 *
 * Created by Victor Garcia on 28/11/16.
 * Email: pfc.vik@gmail.com
 */
public class BaseBean implements Serializable {

    private String folioSerial;

    public String getFolioSerial() {
        return folioSerial;
    }

    public void setFolioSerial(String folioSerial) {
        this.folioSerial = folioSerial;
    }

}
