package com.bancadigital.movil.base;

import java.io.Serializable;

/**
 * Base Bean Response creado para ser implementado por todos los Bean Response en la aplicacion.
 * Contiene variables comunes como regla de negocio.
 *
 * Created by Victor Garcia on 28/11/16.
 * Email: pfc.vik@gmail.com
 */
public class BaseBeanResponse implements Serializable {

    private String codigoRespuestaWS;
    private String mensajeRespuestaWS;

    public String getCodigoRespuestaWS() {
        return codigoRespuestaWS;
    }

    public void setCodigoRespuestaWS(String codigoRespuestaWS) {
        this.codigoRespuestaWS = codigoRespuestaWS;
    }

    public String getMensajeRespuestaWS() {
        return mensajeRespuestaWS;
    }

    public void setMensajeRespuestaWS(String mensajeRespuestaWS) {
        this.mensajeRespuestaWS = mensajeRespuestaWS;
    }
}
