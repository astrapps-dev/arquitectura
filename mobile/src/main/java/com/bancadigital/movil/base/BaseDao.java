/*
 * Copyright (C) 2014 Pedro Vicente Gómez Sánchez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bancadigital.movil.base;

import android.support.annotation.NonNull;
import android.util.Log;

import com.bancadigital.movil.webservices.DaoRequest;
import com.bancadigital.movil.webservices.ServicioWebBean;
import com.bancadigital.movil.Constantes;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Base Dao creado para extender de todos los DAO de la aplicación,
 * resuelve la ejecucion de un request con Retrofit.
 *
 * @author Victor Garcia
 * @email pfc.vik@gmail.com
 */
public class BaseDao {

    /**
     * Ejecuta el endpoint y resuelve si hubo un error en la ejecucion o fue exitosa la llamada,
     * comparando los posible errores y seteandolos en el BeanResponse.
     *
     * @param servicioWebBean El objeto ServicioWebBean, contiene el metodo o request a ejecutar y
     *                        parametro necesarios para el retorno de la respuesta.
     * @return  Devuelve el objeto BeanResponse.
     */
    protected Object consultaServicio(@NonNull ServicioWebBean servicioWebBean) {
        if (servicioWebBean.getEndpoint() == null){
            throw new NullPointerException("The ServicioWebBean Endpoint should not be null");
        }

        servicioWebBean = DaoRequest.requestRetrofit(
                servicioWebBean.getEndpoint(),
                servicioWebBean);

       if (servicioWebBean.getError() != null){
            if (servicioWebBean.getError() instanceof InterruptedException) {
                Log.d("WS_ERROR","InterruptedException");
                ((BaseBeanResponse) servicioWebBean.getResponse()).setCodigoRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[0]);
                ((BaseBeanResponse) servicioWebBean.getResponse()).setMensajeRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[1]);
            } else if (servicioWebBean.getError() instanceof ExecutionException){
                Log.d("WS_ERROR","ExecutionException");
                ((BaseBeanResponse) servicioWebBean.getResponse()).setCodigoRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[0]);
                ((BaseBeanResponse) servicioWebBean.getResponse()).setMensajeRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[1]);
            } else if (servicioWebBean.getError() instanceof TimeoutException){
                Log.d("WS_ERROR","TimeoutException");
                ((BaseBeanResponse) servicioWebBean.getResponse()).setCodigoRespuestaWS(Constantes.CODIGO_ERROR_TIMEOUT[0]);
                ((BaseBeanResponse) servicioWebBean.getResponse()).setMensajeRespuestaWS(Constantes.CODIGO_ERROR_TIMEOUT[1]);
            } else if (servicioWebBean.getError() instanceof HttpException) {
                HttpException response = (HttpException) servicioWebBean.getError();
                Log.d("WS_ERROR","Codigo: " + response.code() + " Mensaje: " + response.getMessage());
                ((BaseBeanResponse) servicioWebBean.getResponse()).setCodigoRespuestaWS(String.valueOf(response.code()));
                ((BaseBeanResponse) servicioWebBean.getResponse()).setMensajeRespuestaWS(response.message());
            } else {
                ((BaseBeanResponse) servicioWebBean.getResponse()).setCodigoRespuestaWS(Constantes.CODIGO_ERROR_CONEXION[0]);
                ((BaseBeanResponse) servicioWebBean.getResponse()).setMensajeRespuestaWS(servicioWebBean.getError().getMessage());
            }
        }

        return servicioWebBean.getResponse();
    }
}
