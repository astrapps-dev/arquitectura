package com.bancadigital.movil.base;

import android.support.annotation.IdRes;

/**
 * Interface creada para ser implementada por todos loas Activities de la aplicacion.
 * Indispensable para la comunicacion entre un fragment y su activity contenedora.
 *
 * @author Victor Garcia
 * @email pfc.vik@gmail.com
 */
public interface FragmentListener {

    /**
     * Metodo para realizar el cambio de un fragment,
     * siempre se ejecuta desde una Activity contenedora.
     *
     * @param fragment          Instancia del fragment que se va a mostrar.
     * @param containerViewId   Id del contenedor, normalmente: R.id.contenedor_principal
     * @param agregarPila       Booleano para decidir si ese fragment se agrega a la pila o no
     */
    void onChangeFragment(BaseFragment fragment, @IdRes int containerViewId, boolean agregarPila);
}
