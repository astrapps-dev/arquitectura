package com.bancadigital.movil.base;

import com.bancadigital.movil.BancaMovilApplication;
import com.bancadigital.movil.modulos.token.bean.TokenBean;
import com.bancadigital.movil.modulos.token.beanresponse.TokenBeanResponse;
import com.bancadigital.movil.modulos.token.service.TokenService;
import com.bancadigital.movil.Constantes;
import com.bancadigital.movil.utilidades.NetworkUtility;
import com.bancadigital.movil.webservices.BancaMovilApi;

/**
 * Base Service creado para ser extendido por cada clase servicio en esta aplicación.
 * Esta clase proporciona metodos y variables necesarias para las reglas de negocio.
 *
 * @author Victor Garcia
 * @email pfc.vik@gmail.com
 */
public abstract class BaseService {

    /**
     *  Booleana seteando si hay conexión a internet o no.
     */
    protected boolean isOnline;

    /**
     *  Variable necesaria cuando se implemente algun tipo de autorizacion o token.
     */
    protected boolean hasAuthorization;

    /**
     *  Variable con la interface de endpoints de la aplicacion,
     *  Interface necesaria para el funcionamiento de request con Retrofit.
     */
    protected BancaMovilApi api;

    /**
     *  Instancia de la respuesta de una autorizacion con Freja Mobile,
     *  seteada en {@link #autorizarTransaccionFreja(String)}
     */
    private TokenService tokenService;

    /**
     *  Obligatoriamente llamar a este metodo al iniciar un nuevo metodo de un servicio.
     *
     */
    protected void onStartService(){
        api = BancaMovilApplication.getApi();
        isOnline = NetworkUtility.isOnline(BancaMovilApplication.getContext());

        // TODO Validar autorizacion consultando al DAO correspondiente, por efectos de prueba se devuelve true
        hasAuthorization = true;
    }

    /**
     *
     * @param folioSerial   Numero de folio para la autorizacion de la transaccion con Freja
     * @return              Retorna la respuesta al consultar la autorizacion de la transaccion.
     */
    protected boolean autorizarTransaccionFreja(String folioSerial){

        tokenService = TokenService.getInstance();

        // Autorizar Transaccion con Freja
        TokenBean tokenBean = new TokenBean();
        tokenBean.setFolioSerial(folioSerial);

        TokenBeanResponse tokenBeanResponse;
        tokenBeanResponse = tokenService.solicitarToken(tokenBean);

        return tokenBeanResponse.getCodigoRespuestaWS().equals(Constantes.CODIGO_EXITO[0]);
    }
}
