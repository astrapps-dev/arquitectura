package com.bancadigital.movil.base;

import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.bancadigital.movil.R;
import com.bancadigital.movil.modulos.inicio.vista.activity.InicioActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Base Activity creado para ser extendido por cada activity en esta aplicación. Esta clase proporciona
 * Configuración de la biblioteca Android ButterKnife y algunos métodos comúnes en cada actividad.
 *
 * @author Victor Garcia
 * @email pfc.vik@gmail.com
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected FragmentManager fragmentManager = getSupportFragmentManager();

    @Nullable @BindView(R.id.toolbar) Toolbar toolbar;

    DialogFragmentInterface dialogFragmentInterface;
    BaseFragment dialogFragmentOpen;

    public interface DialogFragmentInterface {
        void onDialogFragmentResponse(Object object);
    }

    /**
     * Metodo para realizar el cambio de un fragment,
     * siempre se ejecuta desde una Activity contenedora.
     *
     * @param fragmentoGenerico Instancia del fragment que se va a mostrar.
     * @param containerViewId   Id del contenedor, normalmente: R.id.contenedor_principal
     * @param agregarPila       Booleano para decidir si ese fragment se agrega a la pila o no
     */
    private void setFragment(BaseFragment fragmentoGenerico, @IdRes int containerViewId, boolean agregarPila){
        if (!isFinishing()) {
            if (fragmentoGenerico != null) {
                if (agregarPila) {
                    fragmentManager
                            .beginTransaction()
                            .setCustomAnimations(R.anim.left_in, R.anim.left_out)
                            .replace(containerViewId, fragmentoGenerico)
                            .addToBackStack(null)
                            .commit();
                } else {
                    fragmentManager
                            .beginTransaction()
                            .setCustomAnimations(R.anim.left_in, R.anim.left_out)
                            .replace(containerViewId, fragmentoGenerico)
                            .commit();
                }

                if (!(this instanceof InicioActivity)) {
                    setBackButton();
                }

            } else {
                throw new NullPointerException("El fragmento should not be null");
            }
        }
   }

    public void showDialogFragment(BaseFragment fragmentoGenerico, DialogFragmentInterface dialogFragmentInterface){
        if (fragmentoGenerico != null) {
            fragmentoGenerico.setFragmentListener(this);
            dialogFragmentOpen = fragmentoGenerico;
            this.dialogFragmentInterface = dialogFragmentInterface;
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.remove(fragmentoGenerico);
            ft.addToBackStack(null);

            // Create and show the dialog.
            fragmentoGenerico.show(ft, "fragmentDialog");
        } else {
            throw new NullPointerException("El fragmento should not be null");
        }
    }

    public void onChangeFragment(BaseFragment fragment, @IdRes int containerViewId, boolean agregarPila) {
        fragment.setFragmentListener(this);
        setFragment(fragment, containerViewId, agregarPila);
    }

    public void setDialogFragmentResponse(Object object){
        dialogFragmentOpen.dismiss();
        dialogFragmentInterface.onDialogFragmentResponse(object);
    }

    /**
     *  Configura la libreria ButterKnife para la inyección de views
     *  y configura el ActionBar ya que se inyectaron.
     *
     * @param layoutResID Se para como parametro el layout para inflar la actividad, ejemplo:
     *                    R.layout.my_activity
     */
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        setupActionBar();
    }

    /**
     *  Configura el boton de back del toolbar, necesario para la correcta navegacion entre
     *  fragments y activities
     */
    private void setBackButton(){
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            int back = fragmentManager.getBackStackEntryCount();
            if (back > 0) {
                bar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    /**
     *  Configura el {@link #toolbar} como actionbar.
     */
    protected void setupActionBar()
    {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    /**
     *  Implementa la logica de navegacion entre fragmentos y actividades,
     *  al pulsar el boton "back"
     */
    @Override
    public void onBackPressed() {
        int back = fragmentManager.getBackStackEntryCount();
        if (back < 2) {
            finish();
        } else {
            if (!(this instanceof InicioActivity)) {
                ActionBar bar = getSupportActionBar();
                if (back < 3) {
                    if (bar != null) {
                        bar.setDisplayHomeAsUpEnabled(false);
                    }
                }
            }
            super.onBackPressed();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (dialogFragmentOpen != null) {
            dialogFragmentOpen.dismiss();
        }
    }
}
